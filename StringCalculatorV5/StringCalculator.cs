﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculatorV5
{
    public class StringCalculator
    {
        private  List<string> constentVariables=new List<string>(new string[] { "/", "\n",",", "//[", "//", "]","[" });
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            var delimiterList = GetDelimiters(numbers);
            var numberArray = GetNumberArray(numbers, delimiterList);

            CheckInvalidDelimiters(numberArray);

            var convertedNumbList = GetConvertedNumberList(numberArray);

            CheckNegativeNumbers(convertedNumbList);

            convertedNumbList = IngoreNumberGreaterThanThousand(convertedNumbList);

            var total = GetTotal(convertedNumbList);

            return total;
        }
        private List<string> GetDelimiters(string numbers)
        {
            var delimiterList = new List<string>(new string[] { constentVariables[2], constentVariables[1] });

            if (numbers.Contains(constentVariables[3]))
            {
                delimiterList.Clear();

                var delimiters = numbers.Substring(numbers.IndexOf(constentVariables[6]), numbers.LastIndexOf(constentVariables[5]) - 1);

                foreach (var delimiter in delimiters.Split(constentVariables[5].ToCharArray()[0], constentVariables[5].ToCharArray()[0]))
                {
                    delimiterList.Add(delimiter);
                }

            }
            return delimiterList;
        }

        private string[] GetNumberArray(string numbers, List<string> delimiterList)
        {
            if (numbers.Contains(constentVariables[4]))
            {
                numbers = string.Concat(numbers.Split(constentVariables[0].ToCharArray()[0]));
                var delimiters = numbers.Substring(0, numbers.IndexOf(constentVariables[1]));
                delimiterList.Add(delimiters);
                numbers = string.Concat(numbers.Split(constentVariables[0].ToCharArray()[0], constentVariables[1].ToCharArray()[0]));
            }
            if (numbers.Contains(constentVariables[3]))
            {
                numbers = string.Concat(numbers.Split(constentVariables[0].ToCharArray()[0], constentVariables[1].ToCharArray()[0], constentVariables[6].ToCharArray()[0], constentVariables[5].ToCharArray()[0]));
            }
            return numbers.Split(delimiterList.ToArray(), StringSplitOptions.RemoveEmptyEntries);
        }

        private void CheckInvalidDelimiters(string[] numbArray)
        {
            var numberCharArray = string.Concat(numbArray).ToCharArray();
            var invalidDelimiters = string.Empty;

            foreach (var number in numberCharArray)
            {
                try
                {
                    GetConvetedNumber(number.ToString());
                }
                catch
                {
                    if (number!='-') 
                    {
                        invalidDelimiters = string.Join(" ", invalidDelimiters, number);
                    }
                }
            }

            if (!string.IsNullOrEmpty(invalidDelimiters))
            {
                throw new Exception(string.Concat("Invalid Delimiter !.", invalidDelimiters));
            }
        }
       private int GetConvetedNumber(string number)
        {
            return int.Parse(number);
        }
        private List<int> GetConvertedNumberList(string[] numbArray)
        {
            var numberList = new List<int>();

            foreach (var number in numbArray)
            {
                numberList.Add(GetConvetedNumber(number));
            }
            return numberList;
        }

        private void CheckNegativeNumbers(List<int> numberlist)
        {
            var negativeNumbers = string.Empty;

            foreach (var number in numberlist)
            {
                if (number < 0)
                {
                    negativeNumbers = string.Join(" ", negativeNumbers, number);
                }
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                throw new Exception("Negatives not Allowed." + negativeNumbers);
            }
        }

        private List<int> IngoreNumberGreaterThanThousand(List<int> convertedNumbersList)
        {
            var numberList = new List<int>();

            foreach (var number in convertedNumbersList)
            {
                if (number <= 1000)
                {
                    numberList.Add(number);
                }
            }

            return numberList;
        }

        private int GetTotal(List<int> numbersList)
        {
            var sum = 0;
            numbersList.ForEach(l => sum += l);
            return sum;
        }
    }
}
